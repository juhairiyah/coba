import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import React from "react";
import { PhotoCoffe, PhotoProfile } from "../../assets/images";
import {
  Coffe,
  Coffe2,
  Filter,
  Location,
  Notification,
  Search,
} from "../../assets";
import { TextInput } from "react-native-gesture-handler";

const Home = () => {
  return (
    <View style={{ flex: 1 }}>
      {/* Top Menu */}
      <View
        style={{
          flexDirection: "row",
          marginTop: 30,
          marginHorizontal: 30,
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <TouchableOpacity activeOpacity={0.7}>
          <Image source={PhotoProfile} />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ flexDirection: "row", alignItems: "center" }}
        >
          <Image source={Location} />
          <Text style={{ fontWeight: "500", fontSize: 12, marginLeft: 5 }}>
            Jakarta, Indonesia
          </Text>
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={0.7}>
          <Image source={Notification} />
        </TouchableOpacity>
      </View>
      {/* End Top Menu */}
      <View style={{ marginHorizontal: 30, marginTop: 15 }}>
        <Text style={{ fontWeight: "500", fontSize: 14 }}>
          Good Morning, John Doe
        </Text>
      </View>
      {/* Search */}
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "white",
          marginHorizontal: 30,
          paddingHorizontal: 20,
          justifyContent: "space-between",
          alignItems: "center",
          paddingVertical: 5,
          borderRadius: 30,
          marginTop: 20,
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Image source={Search} />
          <TextInput
            placeholder="Search Coffe ..."
            style={{ marginLeft: 15 }}
          />
        </View>
        <View>
          <Image source={Filter} />
        </View>
      </View>
      {/* End Search */}
      {/* Categories */}
      <View style={{ marginLeft: 30, marginTop: 30 }}>
        <View style={{ marginBottom: 15 }}>
          <Text style={{ fontWeight: "500" }}>Categories</Text>
        </View>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {/* Menu */}
          <TouchableOpacity
            activeOpacity={0.7}
            style={{
              backgroundColor: "#00582F",
              paddingVertical: 5,
              paddingHorizontal: 10,
              flexDirection: "row",
              alignItems: "center",
              borderRadius: 30,
              marginHorizontal: 10,
            }}
          >
            <Image source={Coffe} />
            <Text style={{ color: "white", marginLeft: 5 }}>Cappucino</Text>
          </TouchableOpacity>
          {/* End Menu */}
          {/* Menu */}
          <TouchableOpacity
            activeOpacity={0.7}
            style={{
              backgroundColor: "white",
              paddingVertical: 5,
              paddingHorizontal: 10,
              flexDirection: "row",
              alignItems: "center",
              borderRadius: 30,
              elevation: 1,
              shadowColor: "black",
              marginHorizontal: 5,
            }}
          >
            <Image source={Coffe2} />
            <Text style={{ color: "#00582F", marginLeft: 5 }}>Coffe</Text>
          </TouchableOpacity>
          {/* End Menu */}
          {/* Menu */}
          <TouchableOpacity
            activeOpacity={0.7}
            style={{
              backgroundColor: "white",
              paddingVertical: 5,
              paddingHorizontal: 10,
              flexDirection: "row",
              alignItems: "center",
              borderRadius: 30,
              elevation: 1,
              shadowColor: "black",
              marginHorizontal: 5,
            }}
          >
            <Image source={Coffe2} />
            <Text style={{ color: "#00582F", marginLeft: 5 }}>Expresso </Text>
          </TouchableOpacity>
          {/* End Menu */}
          {/* Menu */}
          <TouchableOpacity
            activeOpacity={0.7}
            style={{
              backgroundColor: "white",
              paddingVertical: 5,
              paddingHorizontal: 10,
              flexDirection: "row",
              alignItems: "center",
              borderRadius: 30,
              elevation: 1,
              shadowColor: "black",
              marginHorizontal: 5,
            }}
          >
            <Image source={Coffe2} />
            <Text style={{ color: "#00582F", marginLeft: 5 }}>Cappucino</Text>
          </TouchableOpacity>
          {/* End Menu */}
        </ScrollView>
      </View>
      {/* End Categories */}

      {/* Product */}
      <ScrollView>
        <View>
          <Image source={PhotoCoffe} />
        </View>
      </ScrollView>
      {/* End Product */}
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({});
